/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common;

import com.ming.common.dynamic.code.IClassExecuter;
import com.ming.common.dynamic.code.IStringCompiler;
import com.ming.common.dynamic.code.RunClassHandler;
import com.ming.common.dynamic.code.config.BaseProperties;
import com.ming.common.dynamic.code.config.RunClassProperties;
import com.ming.common.dynamic.code.core.ClassExecuter;
import com.ming.common.dynamic.code.core.JaninoCompiler;
import com.ming.common.dynamic.code.dto.ExecuteResult;
import com.ming.common.dynamic.code.dto.Parameters;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 执行Java代码测试
 * 如果只需要编译一次（源码不会变的情况），建议实例化全局唯一的RunClassHandler对象。
 */
public class RunClassHandlerTest {

    private Logger log = Logger.getLogger(RunClassHandlerTest.class.getName());

    /**
     * 预编译测试
     * @throws Exception 
     */
    //@Test
    public void testRunClass() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        RunClassProperties calTime = new RunClassProperties();
        calTime.setCalExecuteTime(true);
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();
        RunClassHandler handler = new RunClassHandler(compiler, executer, calTime, hackers);

        System.out.println("预编译测试 start");
        
        // 待预编译源码
        List<String> preloadSources = new ArrayList<>();
        preloadSources.add(
                "package com.zhg2yqq.bill;\n"
                + "public class Test {\n"
                + "    public static void main(String[] args) {\n" 
                + "        // 测试打印\n"
                + "        System.out.println(\"normal\");\n"
                + "        System.err.println(\"error\");\n" 
                + "    }\n" 
                + "}");
        preloadSources.add("package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    public String trimStr(String str) {\n"
                + "        return str.trim();\n" 
                + "    }\n" 
                + "}");
        // 预编译
        handler.loadClassFromSources(preloadSources);

        // 执行类
        Parameters args0 = new Parameters();
        String[] pars0 = new String[0];
        args0.add(pars0);
        ExecuteResult result0 = handler.runMethod("com.zhg2yqq.bill.Test", "main", args0);
        Assert.assertNull(result0.getReturnVal());

        Parameters pars1 = new Parameters();
        pars1.add("    测试1     ");
        ExecuteResult result1 = handler.runMethod("com.zhg2yqq.wheels.dynamic.code.CodeTemplate", "trimStr",
                pars1);
        Assert.assertEquals("测试1", result1.getReturnVal());

        handler.loadClassFromSource("package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    public String trimStr(String str) {\n"
                + "        return str.trim() + \"zhg\";\n" 
                + "    }\n" 
                + "}");
        Parameters pars2 = new Parameters();
        pars2.add("  zhg2yqq  测试2");
        ExecuteResult result2 = handler.runMethod("com.zhg2yqq.wheels.dynamic.code.CodeTemplate", "trimStr",
                pars2);
        Assert.assertEquals("zhg2yqq  测试2zhg", result2.getReturnVal());

        System.out.println("预编译测试 end");
    }


    /**
     * 预编译单线程测试
     * @throws Exception
     */
    //@Test
    public void testRunClassSingle() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        BaseProperties calTime = new BaseProperties();
        calTime.setCalExecuteTime(true);
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();

        System.out.println("预编译单线程测试 start");
        RunClassHandler handler = new RunClassHandler(compiler, executer, calTime, hackers);

        // 待预编译源码
        List<String> preloadSources = new ArrayList<>();
        preloadSources.add("package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    private int sum = 0;\n"
                + "    public int add(int num) {\n"
                + "        sum += num;\n"
                + "        return sum;\n"
                + "    }\n"
                + "}");
        // 预编译
        handler.loadClassFromSources(preloadSources);

        int times = 10000;
        // 执行类
        Parameters pars = new Parameters();
        pars.add(1);
        // 非单例测试
        int costTime = 0;
        for (int i = 0; i < times; i++) {
            ExecuteResult result = handler.runMethod("com.zhg2yqq.wheels.dynamic.code.CodeTemplate", "add",
                    pars);
            costTime += result.getRunTakeTime();
            Assert.assertEquals(1, result.getReturnVal());
        }
        System.out.println("非单例执行时间：" + costTime);

        // 单例测试
        costTime = 0;
        for (int i = 0; i < times; i++) {
            ExecuteResult result = handler.runMethod("com.zhg2yqq.wheels.dynamic.code.CodeTemplate", "add",
                    pars, true);
            costTime += result.getRunTakeTime();
            Assert.assertEquals(1 + i, result.getReturnVal());
        }
        System.out.println("单例执行时间：" + costTime);

        System.out.println("预编译单线程测试 end");
    }

    /**
     * 预编译多线程测试
     * @throws Exception
     */
    //@Test
    public void testRunClassMulti() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        BaseProperties calTime = new BaseProperties();
        calTime.setCalExecuteTime(true);
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();

        System.out.println("预编译多线程测试 start");
        RunClassHandler handler = new RunClassHandler(compiler, executer, calTime, hackers);

        // 待预编译源码
        List<String> preloadSources = new ArrayList<>();
        preloadSources.add("package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    public int add(int num0, int num1) {\n"
                + "        return num0 + num1;\n"
                + "    }\n"
                + "}");
        // 预编译
        handler.loadClassFromSources(preloadSources);

        int times = 10000;
        List<Future<Object>> results = new ArrayList<>(times);
        ExecutorService es = new ThreadPoolExecutor(4, 4, 0, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>());
        // 执行类
        Parameters pars = new Parameters();
        pars.add(1);
        pars.add(99);
        // 非单例测试
        final AtomicLong costTime0 = new AtomicLong(0);
        for (int i = 0; i < times; i++) {
            results.add(es.submit(() -> {
                try {
                    ExecuteResult result = handler.runMethod("com.zhg2yqq.wheels.dynamic.code.CodeTemplate", "add",
                            pars);
                    costTime0.addAndGet(result.getRunTakeTime());
                    return result.getReturnVal();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    return -1;
                }
            }));
        }
        for(Future<Object> future : results) {
            Assert.assertEquals(100, future.get());
        }
        System.out.println("非单例执行时间：" + costTime0.get());

        // 单例测试
        results.clear();
        final AtomicLong costTime1 = new AtomicLong(0);
        for (int i = 0; i < times; i++) {
            results.add(es.submit(() -> {
                try {
                    ExecuteResult result = handler.runMethod("com.zhg2yqq.wheels.dynamic.code.CodeTemplate", "add",
                            pars, true);
                    costTime1.addAndGet(result.getRunTakeTime());
                    return result.getReturnVal();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    return -1;
                }
            }));
        }
        for(Future<Object> future : results) {
            Assert.assertEquals(100, future.get());
        }
        System.out.println("单例执行时间：" + costTime1.get());

        es.shutdown();
        System.out.println("预编译多线程测试 end");
    }
}