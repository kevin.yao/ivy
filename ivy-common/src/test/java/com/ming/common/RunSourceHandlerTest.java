/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common;

import com.ming.common.dynamic.code.IClassExecuter;
import com.ming.common.dynamic.code.IStringCompiler;
import com.ming.common.dynamic.code.RunSourceHandler;
import com.ming.common.dynamic.code.config.RunSourceProperties;
import com.ming.common.dynamic.code.core.ClassExecuter;
import com.ming.common.dynamic.code.core.JaninoCompiler;
import com.ming.common.dynamic.code.dto.ExecuteResult;
import com.ming.common.dynamic.code.dto.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 执行Java代码测试
 * 如果源码可能变化需要重新编译，可实例化RunSourceHandler对象。
 */
public class RunSourceHandlerTest {

    private Logger log = Logger.getLogger(RunSourceHandlerTest.class.getName());

    @Autowired
    private RunSourceHandler runSourceHandler;

    /**
     * 源码测试
     * @throws Exception 
     */
    //@Test
    public void testRunSource() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        RunSourceProperties calTime = new RunSourceProperties();
        calTime.setCalCompileTime(true);
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, calTime, hackers);

        System.out.println("源码测试 start");

        // 执行源码
//        Parameters args0 = new Parameters();
//        String[] pars0 = new String[0];
//        args0.add(pars0);
//        ExecuteResult result0 = handler.runMethod("/*\n"
//                + " * Copyright (c) zhg2yqq Corp.\n"
//                + " * All Rights Reserved.\n"
//                + " */\n"
//                + "package com.zhg2yqq.bill;\n"
//                + "public class Test {\n"
//                + "    public static void main(String[] args) {\n"
//                + "        // 测试打印\n"
//                + "        System.out.println(\"normal\");\n"
//                + "        System.err.println(\"error\");\n"
//                + "    }\n"
//                + "}",
//                "main", args0);
//        Assert.assertNull(result0.getReturnVal());

        Parameters pars1 = new Parameters();
        pars1.add("    测试1     ");
        ExecuteResult result1 = handler.runMethod(
                "package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    public String trimStr(String str) {\n"
                + "        return str.trim();\n" 
                + "    }\n" 
                + "}", "trimStr",
                pars1);
        Assert.assertEquals("测试1", result1.getReturnVal());
        System.out.println(result1.getReturnVal());

        // 重新编译加载源码类
        Parameters pars2 = new Parameters();
        pars2.add("    测试2     ");
        ExecuteResult result2 = handler.runMethod(
                "package com.zhg2yqq.wheels.dynamic.code;\n" 
                + "public class CodeTemplate {\n"
                + "    public String trimStr(String str) {\n"
                + "        return str + \"zhg2yqq\";\n" 
                + "    }\n" 
                + "}",
                "trimStr", pars2, false, true);
        Assert.assertEquals("    测试2     zhg2yqq", result2.getReturnVal());
        System.out.println(result2.getReturnVal());

//        result1 = handler.runMethod(
//                "package com.zhg2yqq.wheels.dynamic.code;\n"
//                + "public class CodeTemplate {\n"
//                + "    public String trimStr(String str) {\n"
//                + "        return str.trim();\n"
//                + "    }\n"
//                + "}", "trimStr", pars1);
//        Assert.assertEquals("    测试1     zhg2yqq", result1.getReturnVal());
//        System.out.println(result1.getReturnVal());

        System.out.println("源码测试 end");
    }

    /**
     * 源码单线程测试
     * @throws Exception
     */
    //@Test
    public void testRunSourceSingle() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        RunSourceProperties calTime = new RunSourceProperties();
        calTime.setCalExecuteTime(true);
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();

        System.out.println("源码单线程测试 start");
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, calTime, hackers);

        int times = 10000;
        String source = "package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    private int sum = 0;\n"
                + "    public int add(int num) {\n"
                + "        sum += num;\n"
                + "        return sum;\n"
                + "    }\n"
                + "}";
        // 执行源码
        Parameters args = new Parameters();
        args.add(1);
        // 非单例测试
        int costTime = 0;
        for (int i = 0; i < times; i++) {
            ExecuteResult result = handler.runMethod(source, "add", args);
            costTime += result.getRunTakeTime();
            Assert.assertEquals(1, result.getReturnVal());
        }
        System.out.println("非单例执行时间：" + costTime);

        // 单例测试
        costTime = 0;
        for (int i = 0; i < times; i++) {
            ExecuteResult result = handler.runMethod(source, "add", args, true);
            costTime += result.getRunTakeTime();
            Assert.assertEquals(1 + i, result.getReturnVal());
        }
        System.out.println("单例执行时间：" + costTime);

        // 重新编译
        costTime = 0;
        handler.loadClassFromSource("package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    private int sum = 0;\n"
                + "    public int add(int num) {\n"
                + "        sum += (num << 1);\n"
                + "        return sum;\n"
                + "    }\n"
                + "}");
        for (int i = 0; i < times; i++) {
            ExecuteResult result = handler.runMethod(source, "add", args, true);
            costTime += result.getRunTakeTime();
            Assert.assertEquals(2 * (i + 1), result.getReturnVal());
        }
        System.out.println("重新编译执行时间：" + costTime);

        System.out.println("源码单线程测试 end");
    }

    /**
     * 源码多线程测试
     * @throws Exception
     */
    //@Test
    public void testRunSourceMulti() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        RunSourceProperties calTime = new RunSourceProperties();
        calTime.setCalExecuteTime(true);
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();

        System.out.println("源码多线程测试 start");
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, calTime, hackers);

        // 待预编译源码
        String source = "package com.zhg2yqq.wheels.dynamic.code;\n"
                + "public class CodeTemplate {\n"
                + "    public int add(int num0, int num1) {\n"
                + "        return num0 + num1;\n"
                + "    }\n"
                + "}";

        int times = 10000;
        List<Future<Object>> results = new ArrayList<>(times);
        ExecutorService es = new ThreadPoolExecutor(4, 4, 0, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>());
        // 执行类
        Parameters pars = new Parameters();
        pars.add(1);
        pars.add(99);
        // 非单例测试
        final AtomicLong costTime0 = new AtomicLong(0);
        for (int i = 0; i < times; i++) {
            results.add(es.submit(() -> {
                try {
                    ExecuteResult result = handler.runMethod(source, "add", pars);
                    costTime0.addAndGet(result.getRunTakeTime());
                    return result.getReturnVal();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    return -1;
                }
            }));
        }
        for(Future<Object> future : results) {
            Assert.assertEquals(100, future.get());
        }
        System.out.println("非单例执行时间：" + costTime0.get());

        // 单例测试
        results.clear();
        final AtomicLong costTime1 = new AtomicLong(0);
        for (int i = 0; i < times; i++) {
            results.add(es.submit(() -> {
                try {
                    ExecuteResult result = handler.runMethod(source, "add", pars, true);
                    costTime1.addAndGet(result.getRunTakeTime());
                    return result.getReturnVal();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    return -1;
                }
            }));
        }
        for(Future<Object> future : results) {
            Assert.assertEquals(100, future.get());
        }
        System.out.println("单例执行时间：" + costTime1.get());

        es.shutdown();
        System.out.println("源码多线程测试 end");
    }
}