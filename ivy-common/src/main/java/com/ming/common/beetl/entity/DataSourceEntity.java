package com.ming.common.beetl.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataSourceEntity {

    private String driverClassName;
    private String jdbcUrl;
    private String username;
    private String password;

    public DataSourceEntity() {
    }

    public DataSourceEntity(String driverClassName, String jdbcUrl, String username, String password) {
        this.driverClassName = driverClassName;
        this.jdbcUrl = jdbcUrl;
        this.username = username;
        this.password = password;
    }
}
