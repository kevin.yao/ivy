package com.ming.common.beetl.controller;

import com.ming.common.beetl.service.MdaoSQLManagerService;
import com.ming.common.beetl.util.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/sqlManager")
public class MdaoSQLManagerController {

    @Resource
    private MdaoSQLManagerService mdaoSQLManagerService;

    @GetMapping("/selectAll")
    public Result selectAll(){
        return Result.OK(mdaoSQLManagerService.selectAll());
    }

    @PostMapping("/insert")
    public Result insert(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoSQLManagerService.insert(data));
    }

    @PostMapping("/updateById")
    public Result updateById(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoSQLManagerService.updateById(data));
    }

    @PostMapping("/deleteById")
    public Result deleteById(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoSQLManagerService.deleteById(data));
    }
}
