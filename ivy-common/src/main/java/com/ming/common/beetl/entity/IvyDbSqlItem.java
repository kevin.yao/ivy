package com.ming.common.beetl.entity;

import com.ming.common.anno.Describe;
import com.ming.common.anno.DescribeItem;
import com.ming.common.generate.template.annotation.Generate;
import com.ming.common.generate.template.annotation.database.Column;
import com.ming.common.generate.template.annotation.database.PrimaryKey;
import com.ming.common.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
public class IvyDbSqlItem {

    private Integer index;
    private String type;
    private String column;

    private Integer startRow;
    private Integer pageSize;

}
