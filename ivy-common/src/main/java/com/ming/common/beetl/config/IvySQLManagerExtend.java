package com.ming.common.beetl.config;

import org.beetl.sql.core.SQLManagerExtend;

public class IvySQLManagerExtend extends SQLManagerExtend {

    protected IvyParaExtend paraExtend = new IvyParaExtend();

    public IvySQLManagerExtend(){

    }
    public IvyParaExtend getParaExtend(){
        return  paraExtend;
    }

    public void setParaExtend(IvyParaExtend paraExtend) {
        this.paraExtend = paraExtend;
    }

}
