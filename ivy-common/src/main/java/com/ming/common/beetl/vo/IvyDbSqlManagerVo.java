package com.ming.common.beetl.vo;

import com.ming.common.Options;
import com.ming.common.beetl.entity.IvyDbSqlManager;
import lombok.Data;

@Data
public class IvyDbSqlManagerVo extends IvyDbSqlManager {

    private Options options;

    private String sourceSql;

}
