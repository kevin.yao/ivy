package com.ming.common.beetl.vo;

import com.ming.common.Options;
import com.ming.common.beetl.entity.IvyDbSqlManager;
import lombok.Data;

@Data
public class IvyDbTableVo {

    private Options options;

    private Long id;

    private String tableName;

}
