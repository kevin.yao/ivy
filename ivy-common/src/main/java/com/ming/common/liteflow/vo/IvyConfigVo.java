package com.ming.common.liteflow.vo;

import com.ming.common.Options;
import com.ming.common.liteflow.core.config.IvyConfig;
import lombok.Data;

@Data
public class IvyConfigVo extends IvyConfig {

    private Options options;

}
