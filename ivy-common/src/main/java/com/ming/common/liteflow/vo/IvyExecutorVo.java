package com.ming.common.liteflow.vo;

import com.ming.common.Options;
import com.ming.common.liteflow.core.flowexecutor.IvyExecutor;
import lombok.Data;

@Data
public class IvyExecutorVo extends IvyExecutor {

    private Options options;

}
