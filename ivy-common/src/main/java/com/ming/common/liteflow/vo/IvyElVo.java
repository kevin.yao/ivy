package com.ming.common.liteflow.vo;

import com.ming.common.Options;
import com.ming.common.liteflow.core.el.IvyEl;
import lombok.Data;

@Data
public class IvyElVo extends IvyEl {

    private Options options;

}
