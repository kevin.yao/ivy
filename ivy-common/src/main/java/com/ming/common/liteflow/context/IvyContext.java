package com.ming.common.liteflow.context;

import com.ming.common.liteflow.core.chain.IvyChain;
import com.ming.common.liteflow.core.config.IvyConfig;
import com.ming.common.liteflow.core.el.IvyEl;
import com.ming.common.liteflow.core.flowexecutor.IvyExecutor;
import com.ming.common.xxljob.context.XxlJobContext;
import com.yomahub.liteflow.core.FlowExecutor;
import lombok.Data;

@Data
public class IvyContext {

    private IvyChain ivyChain;
    private IvyEl ivyEl;
    private IvyExecutor ivyExecutor;
    private IvyConfig ivyConfig;
    private FlowExecutor flowExecutor;
    private XxlJobContext xxlJobContext;

}
