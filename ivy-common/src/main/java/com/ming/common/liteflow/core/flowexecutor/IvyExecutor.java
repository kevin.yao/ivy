package com.ming.common.liteflow.core.flowexecutor;

import com.ming.common.anno.Describe;
import com.ming.common.anno.DescribeItem;
import com.ming.common.generate.template.annotation.Generate;
import com.ming.common.generate.template.annotation.database.Column;
import com.ming.common.generate.template.annotation.database.PrimaryKey;
import com.ming.common.liteflow.core.config.IvyConfig;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_executor")
@Generate(isEffective = true, moduleName = "db", desc = "执行器")
public class IvyExecutor {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    @Describe(value = "执行器ID")
    private String executorId;

    @Column
    @Describe(value = "执行器名称")
    private String executorName;

    @Column(len = 12)
    @Describe(value = "执行器类型",items = {
        @DescribeItem(value = "execute2Resp",desc = "execute2Resp"),
        @DescribeItem(value = "execute2Future",desc = "execute2Future"),
    })
    private String executorType;

    @Column
    @Describe(value = "执行器配置IvyConfig")
    private Long ivyConfigId;

    private IvyConfig ivyConfig;

}
