package com.ming.common.liteflow.core.flowexecutor;

import com.ming.common.liteflow.core.config.IvyConfig;
import com.ming.common.liteflow.core.config.IvyConfigUtil;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.core.FlowExecutorHolder;
import com.yomahub.liteflow.property.LiteflowConfig;

public class FlowExecutorUtil {

    public static FlowExecutor getFlowExecutor(){
        return FlowExecutorHolder.loadInstance(IvyConfigUtil.getDefaultConfig());
    }

    public static FlowExecutor getFlowExecutor(IvyConfig conf){
        return FlowExecutorHolder.loadInstance(IvyConfigUtil.getConfig(conf));
    }

    public static FlowExecutor getFlowExecutor(LiteflowConfig config){
        return FlowExecutorHolder.loadInstance(config);
    }
}
