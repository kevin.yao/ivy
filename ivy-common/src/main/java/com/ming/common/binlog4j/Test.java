package com.ming.common.binlog4j;

import lombok.Data;

@Data
public class Test {

    private Integer id;
    private String name;
    private Integer age;

}
