package com.ming.common.util;

import com.ming.common.dynamic.code.factory.StandardCompilerFactory;
import com.ming.common.xxljob.util.IpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FileUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    public static void removeSameFile(String rootPath,String sameName,String... fileNames){
        // 强制删除文件夹及其内容
        Set<String> fileSet = new HashSet<>(Arrays.asList(fileNames));
        try {
            Files.walk(Paths.get(rootPath.substring(1)))
                    .sorted((p1, p2) -> -p1.compareTo(p2)) // 逆序遍历，确保先删除子文件夹和文件
                    .forEach(p -> {
                        try {
                            String f = p.getFileName().toString();
                            if(fileSet.contains(f) || f.startsWith(sameName)){
                                Files.delete(p);
                                logger.info("删除文件：{}{}",rootPath,f);
                            }
                        } catch (Exception e) {
                            // 异常处理
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void removeFile(String rootPath,String... fileNames){
        // 强制删除文件夹及其内容
        Set<String> fileSet = new HashSet<>(Arrays.asList(fileNames));
        try {
            Files.walk(Paths.get(rootPath.substring(1)))
                    .sorted((p1, p2) -> -p1.compareTo(p2)) // 逆序遍历，确保先删除子文件夹和文件
                    .forEach(p -> {
                        try {
                            String f = p.getFileName().toString();
                            if(fileSet.contains(f)){
                                Files.delete(p);
                                logger.info("删除文件：{}{}",rootPath,f);
                            }
                        } catch (Exception e) {
                            // 异常处理
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void removeAllFile(String rootPath){
        // 强制删除文件夹及其内容
        try {
            Files.walk(Paths.get(rootPath.substring(1)))
                    .sorted((p1, p2) -> -p1.compareTo(p2)) // 逆序遍历，确保先删除子文件夹和文件
                    .forEach(p -> {
                        try {
                            String f = p.getFileName().toString();
                            Files.delete(p);
                            logger.info("删除文件：{}{}",rootPath,f);
                        } catch (Exception e) {
                            // 异常处理
                            e.printStackTrace();
                        }
                    });
        } catch (NoSuchFileException e) {
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
