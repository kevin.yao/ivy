package com.ming.common.dynamic.loader.class_loader;

import javax.tools.SimpleJavaFileObject;
import java.net.URI;

/**
 * java源码保存对象(内存：不生成class文件)
 */
public class JavaMemSource extends SimpleJavaFileObject{

    /**
     * java源码
     */
    private String javaSourceCode;

    public JavaMemSource(String name, String javaSourceCode) {
        super(URI.create("string:///" + name.replace('.', '/')+ Kind.SOURCE.extension), Kind.SOURCE);
        this.javaSourceCode = javaSourceCode;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        return javaSourceCode;
    }
}

