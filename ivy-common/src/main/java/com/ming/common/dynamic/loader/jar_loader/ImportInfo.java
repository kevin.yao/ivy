package com.ming.common.dynamic.loader.jar_loader;

public class ImportInfo {

    private String className;

    private String classPath;

    public ImportInfo() {
    }

    public ImportInfo(String className, String classPath) {
        this.className = className;
        this.classPath = classPath;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }
}
