/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.code;

import com.ming.common.dynamic.code.config.BaseProperties;
import com.ming.common.dynamic.code.dto.CompileResult;
import com.ming.common.dynamic.code.exception.CompileException;

import java.util.Map;


/**
 * 源码编译器
 * 
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月11日
 */
public interface IStringCompiler {
    /**
     * 编译源码
     * 
     * @param fullClassName 类全名
     * @param sourceCode 源码
     * @param properties 配置
     * @return 成功编译结果
     * @throws CompileException .
     */
    CompileResult compile(String fullClassName, String sourceCode, BaseProperties properties)
        throws CompileException;

    /**
     * 批量编译源码
     * 
     * @param sources key:类全名 value:源码
     * @param properties 配置
     * @return 成功编译结果
     * @throws CompileException .
     */
    Map<String, CompileResult> compile(Map<String, String> sources, BaseProperties properties)
        throws CompileException;
//    /**
//     * 获取编译器
//     * @return
//     */
//    JavaCompiler getCompiler();
}
