/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.code.dto;

/**
 * class方法执行结果
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月13日
 */
public class ExecuteResult {
    /**
     * 方法执行用时
     */
    private long runTakeTime;
    /**
     * 方法执行完后返回值
     */
    private Object returnVal;
    /**
     * 执行的类与实例信息
     */
    private ClassBean clazzBean;
    public ExecuteResult(ClassBean clazzBean) {
        this.clazzBean = clazzBean;
    }
    public ExecuteResult(ClassBean clazzBean, long runTakeTime, Object returnVal) {
        this.clazzBean = clazzBean;
        this.runTakeTime = runTakeTime;
        this.returnVal = returnVal;
    }
    
    public long getRunTakeTime() {
        return runTakeTime;
    }
    public Object getReturnVal() {
        return returnVal;
    }
    public ClassBean getClazzBean() {
        return clazzBean;
    }
}
