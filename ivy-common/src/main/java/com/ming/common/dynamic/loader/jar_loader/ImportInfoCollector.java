package com.ming.common.dynamic.loader.jar_loader;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.ArrayList;
import java.util.List;

public class ImportInfoCollector extends ClassVisitor {

    private List<ImportInfo> list;

    public ImportInfoCollector() {
        super(Opcodes.ASM9);
        this.list = new ArrayList<>();
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        list.add(new ImportInfo(name.replaceAll("/","."),name));
        for (String iface : interfaces) {
            System.out.println("Imported interface: " + iface);
        }
        /*if (superName != null) {
            System.out.println("Imported superclass: " + superName);
        }*/
        visitConstantClass(name);
    }

    public void visitConstantClass(String name) {
        String importedClassName = Type.getObjectType(name).getClassName();
        System.out.println("Imported class: " + importedClassName);
    }

    public List<ImportInfo> getList() {
        return list;
    }
}
