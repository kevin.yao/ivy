package com.ming.common.ivy;

import com.ming.common.anno.Describe;
import com.ming.common.anno.DescribeItem;
import com.ming.common.generate.template.annotation.Generate;
import com.ming.common.generate.template.annotation.database.Column;
import com.ming.common.generate.template.annotation.database.PrimaryKey;
import com.ming.common.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_dict")
@Generate(isEffective = true, moduleName = "db", desc = "字典表")
public class IvyDict {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    private String code;

    @Text
    @Column
    private String value;

}
