package com.ming.job.xxljob.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "xxl_job_registry")
public class XxlJobRegistry {

    private Integer id;
    private String registryGroup;
    private String registryKey;
    private String registryValue;
    private Date updateTime;

}
