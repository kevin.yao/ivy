package com.ming.job.xxljob;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.ming.job.dao") // Specify the package containing your mappers
public class MyBatisConfig {
    // MyBatis configuration if needed
}
