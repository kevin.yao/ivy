package com.ming.job.xxljob.vo;

import com.ming.common.Options;
import com.ming.job.xxljob.entity.XxlJobGroup;
import lombok.Data;

@Data
public class XxlJobGroupVo extends XxlJobGroup {

    private Options options;

}
