package com.ming.liteflow.controller;

import com.ming.common.Options;
import com.ming.common.SortBy;
import com.ming.common.beetl.util.Result;
import com.ming.common.beetl.util.StrUtil;
import com.ming.common.liteflow.core.chain.IvyChain;
import com.ming.common.liteflow.vo.IvyChainVo;
import com.ming.common.xxljob.annotation.PermissionLimit;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/liteflow/chain")
public class LiteFlowChainController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/options")
    @PermissionLimit(limit = false)
    public Result<?> options(@RequestBody(required = false) Map<String,Object> map) {
        LambdaQuery<IvyChain> query = sqlManager.lambdaQuery(IvyChain.class);
        List<IvyChain> list = query.select(IvyChain::getId,IvyChain::getChainId,IvyChain::getChainName);
        list = list.stream().peek(m->m.setChainName(m.getChainName()+"【"+m.getChainId()+"】")).collect(Collectors.toList());
        return Result.OK(list);
    }

    @PostMapping("/page")
    @PermissionLimit(limit = false)
    public Result<PageResult<IvyChain>> page(@RequestBody IvyChainVo vo){
        LambdaQuery<IvyChain> lambdaQuery = sqlManager.lambdaQuery(IvyChain.class);
        lambdaQuery.andEq(IvyChain::getIvyElId, LambdaQuery.filterEmpty(vo.getIvyElId()));
        lambdaQuery.andLike(IvyChain::getChainId, LambdaQuery.filterLikeEmpty(vo.getChainId()));
        lambdaQuery.andLike(IvyChain::getChainName, LambdaQuery.filterLikeEmpty(vo.getChainName()));
        Options options = vo.getOptions();
        List<SortBy> sortBy = options.getSortBy();
        for (SortBy sort : sortBy) {
            if ("desc".equalsIgnoreCase(sort.getOrder())) {
                lambdaQuery.desc(StrUtil.camelToSnake(sort.getKey()));
            } else {
                lambdaQuery.asc(StrUtil.camelToSnake(sort.getKey()));
            }
        }
        PageResult<IvyChain> page = lambdaQuery.page(options.getPage(), options.getItemsPerPage());
        return Result.OK(page);
    }

    @PostMapping("/add")
    @PermissionLimit(limit = false)
    public Result<?> add(@RequestBody IvyChain item){
        LambdaQuery<IvyChain> query = sqlManager.lambdaQuery(IvyChain.class);
        int i = query.insert(item);
        return Result.OK(i);
    }

    @PostMapping("/update")
    @PermissionLimit(limit = false)
    public Result<Object> update(@RequestBody IvyChain item){
        LambdaQuery<IvyChain> lambdaQuery = sqlManager.lambdaQuery(IvyChain.class);
        lambdaQuery.andNotEq(IvyChain::getId, item.getId());
        lambdaQuery.andEq(IvyChain::getChainId, item.getChainId());
        long count = lambdaQuery.count();
        if(count > 0){
            return Result.error("链路ID重复");
        }
        int i = sqlManager.updateById(item);
        return Result.OK("更新成功", i);
    }

    @PostMapping("/delete")
    @PermissionLimit(limit = false)
    public Result<Integer> delete(@RequestBody IvyChain item){
        LambdaQuery<IvyChain> lambdaQuery = sqlManager.lambdaQuery(IvyChain.class);
        lambdaQuery.andEq(IvyChain::getId, item.getId());
        int i = lambdaQuery.delete();
        return Result.OK("删除成功",i);
    }
}
