package com.ming.liteflow.cmp.script;

import com.yomahub.liteflow.script.ScriptExecuteWrap;
import com.yomahub.liteflow.script.body.JaninoWhileScriptBody;

public class ScriptWhileCmp implements JaninoWhileScriptBody {
    @Override
    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {
        System.out.println("ScriptWhileCmp executed!");
        return true;
    }
}
