package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpSwitchFallback")
public class IvyDynamicCmpSwitchFallback extends NodeSwitchComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpSwitchFallback.class);

    @Override
    public String processSwitch() throws Exception {
        LOG.info("IvyDynamicCmpSwitchFallback executed!");
        System.out.println("IvyDynamicCmpSwitchFallback executed!");
        return "a";
    }
}