package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBreakComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpBreakFallback")
public class IvyDynamicCmpBreakFallback extends NodeBreakComponent {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpBreakFallback.class);

	@Override
	public boolean processBreak() {
		LOG.info("IvyDynamicCmpBreakFallback executed!");
		System.out.println("IvyDynamicCmpBreakFallback executed!");
		return true;
	}
}
