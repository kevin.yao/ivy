package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IvyDynamicClass {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicClass.class);

	public String processTest(String str) {
		LOG.info("IvyDynamicClass executed!");
		System.out.println("IvyDynamicClass executed!");
		return str;
	}

}
