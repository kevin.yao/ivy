package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeIfComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpIfFallback")
public class IvyDynamicCmpIfFallback extends NodeIfComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIfFallback.class);

    @Override
    public boolean processIf() throws Exception {
        LOG.info("IvyDynamicCmpIfFallback executed!");
        System.out.println("IvyDynamicCmpIfFallback executed!");
        return true;
    }
}