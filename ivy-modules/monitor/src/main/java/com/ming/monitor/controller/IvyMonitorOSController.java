package com.ming.monitor.controller;

import com.ming.common.beetl.util.Result;
import org.beetl.sql.core.SQLManager;
import org.beifengtz.jvmm.core.JvmmCollector;
import org.beifengtz.jvmm.core.JvmmFactory;
import org.beifengtz.jvmm.core.entity.info.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/monitor/os")
public class IvyMonitorOSController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/info")
    public Result<?> info(@RequestBody(required = false) Map<String,Object> map) {
        JvmmCollector collector = JvmmFactory.getCollector();
        SysInfo sys = collector.getSys();//操作系统信息
        SysMemInfo sysMem = collector.getSysMem();//操作系统内存数据
        List<SysFileInfo> sysFile = collector.getSysFile();//采集操作系统磁盘分区使用情况数据
        ProcessInfo process = collector.getProcess();//当前进程数据
        List<DiskInfo> disk = collector.getDisk();//物理机磁盘数据
        CompletableFuture<List<DiskIOInfo>> diskIO = collector.getDiskIO();//物理机磁盘IO及吞吐量数据
        CompletableFuture<DiskIOInfo> diskIO1 = collector.getDiskIO("C");//物理机磁盘IO及吞吐量数据
        CompletableFuture<CPUInfo> cpu = collector.getCPU();//物理机CPU负载数据
        CompletableFuture<NetInfo> network = collector.getNetwork();//物理机网卡信息及IO数据
        PortInfo portInfo = collector.getPortInfo(8080);//物理机器端口使用情况



        return Result.OK(collector);
    }


}
