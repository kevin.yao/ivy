package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ThenELWrapper;

public class ELBusThen extends BaseELBus {

    public static ThenELWrapper node(Object... objects){
        return ELBus.then(objects);
    }

    public static ThenELWrapper node(IvyCmp cmp){
        ThenELWrapper wrapper = ELBus.then(ELBusNode.node(cmp));
        wrapper.pre(cmp.getPreEL());
        wrapper.finallyOpt(cmp.getFinallyEL());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
