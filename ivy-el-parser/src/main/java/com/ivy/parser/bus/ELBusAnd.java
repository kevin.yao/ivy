package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.ivy.builder.graph.Node;
import com.ivy.builder.graph.NodeProperties;
import com.yomahub.liteflow.builder.el.AndELWrapper;
import com.yomahub.liteflow.builder.el.ELBus;

public class ELBusAnd extends BaseELBus {

    public static AndELWrapper node(Object... objects){
        return ELBus.and(objects);
    }

    public static AndELWrapper node(IvyCmp cmp){
        AndELWrapper wrapper = ELBus.and(cmp.getAndEL());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

//    public static AndELWrapper node(Node node){
//        NodeProperties properties = node.getProperties();
//        AndELWrapper wrapper = ELBus.and(node.getProperties().getAndEL());
//        setId(wrapper, properties);
//        setTag(wrapper, properties);
//        setMaxWaitSeconds(wrapper, properties);
//        return wrapper;
//    }

}
