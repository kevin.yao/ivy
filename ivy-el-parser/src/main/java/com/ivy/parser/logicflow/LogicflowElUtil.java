package com.ivy.parser.logicflow;

import cn.hutool.core.collection.CollUtil;
import com.ivy.builder.graph.LogicFlowData;
import com.ivy.parser.execption.LiteFlowELException;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.ThenELWrapper;
import com.yomahub.liteflow.builder.el.WhenELWrapper;

import java.util.List;
import java.util.stream.Collectors;

public class LogicflowElUtil {

    public static ELWrapper transform(LogicFlow flow) throws LiteFlowELException {
        List<LogicFlowData> flowDataList = LogicflowJsonUtil.getLogicFlowGroup(flow.json);
        flow.flowDataList = flowDataList;
        if(flowDataList.size() > 1){
            //多起点嵌套处理
            WhenELWrapper when = ELBus.when();
            for (LogicFlowData data : flowDataList){
                LogicFlowGraphEL graphEL = LogicFlowGraphEL.getGraphEL(data);
                if(data.getNodes().size() == 1 && data.getEdges().size() == 0){
                    when.when(LogicflowExecutor.nodeToEL(data.getNodes().get(0)));
                }else{
                    when.when(logicFlow(graphEL));
                }
                graphEL.setGroupNodeProp(graphEL.getStartNode(), when);//设置分组属性
                flow.graphELList.add(graphEL);
                if(CollUtil.isNotEmpty(data.getIvyCmpList())) {
                    flow.nodeInfoList.addAll(data.getIvyCmpList());
                }
                flow.nodeInfoList = flow.nodeInfoList.stream().distinct().collect(Collectors.toList());
            }
            return when;
        }else if(flowDataList.size() == 1){
            //单起点嵌套处理
            LogicFlowData data = flowDataList.get(0);
            LogicFlowGraphEL graphEL = LogicFlowGraphEL.getGraphEL(data);
            flow.graphELList.add(graphEL);
            if(CollUtil.isNotEmpty(data.getIvyCmpList())){
                flow.nodeInfoList.addAll(data.getIvyCmpList());
            }
            flow.nodeInfoList = flow.nodeInfoList.stream().distinct().collect(Collectors.toList());
            return logicFlow(graphEL);
        }
        return null;
    }

    public static ELWrapper logicFlow(LogicFlowGraphEL graphEL) throws LiteFlowELException {
        ELWrapper wrapper = LogicflowExecutor.elWrapper(graphEL);
        //前置后置组件
        if(wrapper instanceof ThenELWrapper){
            ThenELWrapper thenELWrapper = (ThenELWrapper) wrapper;
            LogicflowExecutor.preELWrapper(thenELWrapper,graphEL);
            LogicflowExecutor.finallyELWrapper(thenELWrapper,graphEL);
        }
        return wrapper;
    }

}
