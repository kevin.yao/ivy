package com.ivy.parser.utils.number;

import java.util.Random;

public class NumberUtil {

    public static Integer index = 0;
    public static final int MAX_VALUE = Integer.MAX_VALUE - 1;

    public static int getNextIndex() {
        // 自增并检查是否超过一亿
        if (index >= MAX_VALUE) {
            index = 0; // 重置为0
        }
        return ++index;
    }

    public static int randomInt(int minRange, int maxRange) {
        Random random = new Random();
        // 生成指定范围的随机整数
        return random.nextInt((maxRange - minRange) + 1) + minRange;
    }

    public static boolean isNumeric(String str) {
        try {
            int result = Integer.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
