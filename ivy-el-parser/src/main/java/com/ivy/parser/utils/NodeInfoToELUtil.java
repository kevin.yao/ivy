package com.ivy.parser.utils;

import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.ivy.builder.graph.NodeProperties;
import com.ivy.parser.bus.*;
import com.ivy.parser.execption.LiteFlowELException;
import com.yomahub.liteflow.builder.el.ELWrapper;

public class NodeInfoToELUtil {
    public static ELWrapper buildELWrapper(NodeProperties nodeInfoWrapper) throws LiteFlowELException {
        return buildELWrapper(nodeInfoWrapper, false);
    }

    public static ELWrapper buildELWrapper(NodeProperties nodeInfoWrapper, boolean isSimple) throws LiteFlowELException {
        String componentId = nodeInfoWrapper.getComponentId();
        if(cn.hutool.core.util.StrUtil.isBlank(componentId)){
            throw new LiteFlowELException("节点组件ID为空");
        }
        return toELWrapper(nodeInfoWrapper,isSimple);
    }

    public static ELWrapper toELWrapper(IvyCmp info){
        return toELWrapper(info,false);
    }

    public static ELWrapper toELWrapper(IvyCmp info, boolean isSimple){
        handler(info);
        ELWrapper el = null;
        switch (info.getType()){
            case "common":
                if(isSimple){
                    el = ELBusNode.node(info);
                }else{
                    el = ELBusThen.node(info);
                }
                break;
            case "switch": el = ELBusSwitch.NEW().node(info).toELWrapper();break;
            case "if": el = ELBusIf.NEW().node(info).toELWrapper();break;
            case "for": el = ELBusFor.node(info);break;
            case "while": el = ELBusWhile.node(info);break;
            case "iterator": el = ELBusIterator.node(info);break;
            case "break": el = ELBusBreak.NEW().node(info).toELWrapper();break;
            case "script":
                if(isSimple) {
                    el = ELBusNode.node(info);
                }else{
                    el = ELBusThen.node(info);
                }
                break;
            case "switch_script": el = ELBusSwitch.NEW().node(info).toELWrapper();break;
            case "if_script": el = ELBusIf.NEW().node(info).toELWrapper();break;
            case "for_script": el = ELBusFor.node(info);break;
            case "while_script": el = ELBusWhile.node(info);break;
            case "break_script": el = ELBusBreak.NEW().node(info).toELWrapper();break;
            case "fallback":
            default:
        }
        return el;
    }

    public static String toEL(IvyCmp info, boolean format){
        return toELWrapper(info).toEL(format);
    }

    public static void handler(IvyCmp info) {
        if(StrUtil.isEmpty(info.getScript())){info.setScript(null);}
        if(StrUtil.isEmpty(info.getLanguage())){info.setLanguage(null);}
        if(StrUtil.isEmpty(info.getClazz())){info.setClazz(null);}
        if(StrUtil.isEmpty(info.getCmpPre())){info.setCmpPre(null);}
        if(StrUtil.isEmpty(info.getCmpFinallyOpt())){info.setCmpFinallyOpt(null);}
        if(StrUtil.isEmpty(info.getCmpId())){info.setCmpId(null);}
        if(StrUtil.isEmpty(info.getCmpTag())){info.setCmpTag(null);}
        if(StrUtil.isEmpty(info.getCmpTo())){info.setCmpTo(null);}
        if(StrUtil.isEmpty(info.getCmpDefaultOpt())){info.setCmpDefaultOpt(null);}
        if(StrUtil.isEmpty(info.getCmpTrueOpt())){info.setCmpTrueOpt(null);}
        if(StrUtil.isEmpty(info.getCmpFalseOpt())){info.setCmpFalseOpt(null);}
        if(StrUtil.isEmpty(info.getCmpDoOpt())){info.setCmpDoOpt(null);}
        if(StrUtil.isEmpty(info.getCmpBreakOpt())){info.setCmpBreakOpt(null);}
        if(StrUtil.isEmpty(info.getCmpDataName())){info.setCmpDataName(null);}
        if(StrUtil.isEmpty(info.getCmpData())){info.setCmpData(null);}
    }

}
