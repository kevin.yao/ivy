package com.ivy.parser.utils;


import cn.hutool.core.util.StrUtil;
import com.ivy.parser.utils.coll.CollUtil;
import com.ivy.parser.utils.number.BigNumberUtil;

public class CommonUtil {

    public static final BigNumberUtil bigNumberUtil = BigNumberUtil.NEW();
    public static final CollUtil collUtil = CollUtil.NEW();

//    public static void main(String[] args) {
//        String num = CommonUtil.bigNumberUtil.add("1.12e12346","2.31e12345");
//        System.out.println(num);
//    }

    public static String[] split(String strs) {
        if(StrUtil.isNotEmpty(strs)){
            return strs.split(",");
        }
        return null;
    }
}
