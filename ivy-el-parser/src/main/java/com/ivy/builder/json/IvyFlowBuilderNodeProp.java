package com.ivy.builder.json;


import com.ivy.builder.graph.NodeProperties;
import com.yomahub.liteflow.enums.NodeTypeEnum;

public class IvyFlowBuilderNodeProp {

    private NodeProperties properties;

    public IvyFlowBuilderNodeProp() {
        this.properties = new NodeProperties();
    }

    public static IvyFlowBuilderNodeProp NEW(){
        return new IvyFlowBuilderNodeProp();
    }

    public IvyFlowBuilderNodeProp ids(String[] ids){ properties.setIds(ids);return this; }
    public IvyFlowBuilderNodeProp whenIgnoreError(boolean whenIgnoreError){ properties.setWhenIgnoreError(whenIgnoreError);return this; }
    public IvyFlowBuilderNodeProp whenAny(boolean whenAny){ properties.setWhenAny(whenAny);return this; }
    public IvyFlowBuilderNodeProp whenMust(String whenMust){ properties.setWhenMust(whenMust);return this; }
    public IvyFlowBuilderNodeProp ignoreType(boolean ignoreType){ properties.setIgnoreType(ignoreType);return this; }
    public IvyFlowBuilderNodeProp fallbackCommonId(Long fallbackCommonId){ properties.setFallbackCommonId(fallbackCommonId);return this; }
    public IvyFlowBuilderNodeProp fallbackSwitchId(Long fallbackSwitchId){ properties.setFallbackSwitchId(fallbackSwitchId);return this; }
    public IvyFlowBuilderNodeProp fallbackIfId(Long fallbackIfId){ properties.setFallbackIfId(fallbackIfId);return this; }
    public IvyFlowBuilderNodeProp fallbackForId(Long fallbackForId){ properties.setFallbackForId(fallbackForId);return this; }
    public IvyFlowBuilderNodeProp fallbackWhileId(Long fallbackWhileId){ properties.setFallbackWhileId(fallbackWhileId);return this; }
    public IvyFlowBuilderNodeProp fallbackBreakId(Long fallbackBreakId){ properties.setFallbackBreakId(fallbackBreakId);return this; }
    public IvyFlowBuilderNodeProp fallbackIteratorId(Long fallbackIteratorId){ properties.setFallbackIteratorId(fallbackIteratorId);return this; }

    public IvyFlowBuilderNodeProp id(Long id){ properties.setId(id);return this; }
    public IvyFlowBuilderNodeProp componentId(String componentId){ properties.setComponentId(componentId);return this; }
    public IvyFlowBuilderNodeProp componentName(String componentName){ properties.setComponentName(componentName);return this; }
    public IvyFlowBuilderNodeProp type(String type){ properties.setType(type);return this; }
    public IvyFlowBuilderNodeProp nodeType(NodeTypeEnum nodeType){ properties.setNodeType(nodeType);properties.setType(nodeType.getCode());return this; }
    public IvyFlowBuilderNodeProp script(String script){ properties.setScript(script);return this; }
    public IvyFlowBuilderNodeProp language(String language){ properties.setLanguage(language);return this; }
    public IvyFlowBuilderNodeProp clazz(String clazz){ properties.setClazz(clazz);return this; }
    public IvyFlowBuilderNodeProp fallbackId(Long fallbackId){ properties.setFallbackId(fallbackId);return this; }
    public IvyFlowBuilderNodeProp fallbackType(String fallbackType){ properties.setFallbackType(fallbackType);return this; }
    public IvyFlowBuilderNodeProp el(String el){ properties.setEl(el);return this; }
    public IvyFlowBuilderNodeProp elFormat(String elFormat){ properties.setElFormat(elFormat);return this; }
    public IvyFlowBuilderNodeProp cmpPre(String cmpPre){ properties.setCmpPre(cmpPre);return this; }
    public IvyFlowBuilderNodeProp cmpFinallyOpt(String cmpFinallyOpt){ properties.setCmpFinallyOpt(cmpFinallyOpt);return this; }
    public IvyFlowBuilderNodeProp cmpId(String cmpId){ properties.setCmpId(cmpId);return this; }
    public IvyFlowBuilderNodeProp cmpTag(String cmpTag){ properties.setCmpTag(cmpTag);return this; }
    public IvyFlowBuilderNodeProp cmpMaxWaitSeconds(Integer cmpMaxWaitSeconds){ properties.setCmpMaxWaitSeconds(cmpMaxWaitSeconds);return this; }
    public IvyFlowBuilderNodeProp cmpTo(String cmpTo){ properties.setCmpTo(cmpTo);return this; }
    public IvyFlowBuilderNodeProp cmpDefaultOpt(String cmpDefaultOpt){ properties.setCmpDefaultOpt(cmpDefaultOpt);return this; }
    public IvyFlowBuilderNodeProp cmpToEL(Object cmpToEL){ properties.setCmpToEL(cmpToEL);return this; }
    public IvyFlowBuilderNodeProp cmpDefaultOptEL(Object cmpDefaultOptEL){ properties.setCmpDefaultOptEL(cmpDefaultOptEL);return this; }
    public IvyFlowBuilderNodeProp cmpTrueOpt(String cmpTrueOpt){ properties.setCmpTrueOpt(cmpTrueOpt);return this; }
    public IvyFlowBuilderNodeProp cmpTrueOptEL(Object cmpTrueOptEL){ properties.setCmpTrueOptEL(cmpTrueOptEL);return this; }
    public IvyFlowBuilderNodeProp cmpFalseOpt(String cmpFalseOpt){ properties.setCmpFalseOpt(cmpFalseOpt);return this; }
    public IvyFlowBuilderNodeProp cmpFalseOptEL(Object cmpFalseOptEL){ properties.setCmpFalseOptEL(cmpFalseOptEL);return this; }
    public IvyFlowBuilderNodeProp cmpParallel(Boolean cmpParallel){ properties.setCmpParallel(cmpParallel);return this; }
    public IvyFlowBuilderNodeProp cmpDoOpt(String cmpDoOpt){ properties.setCmpDoOpt(cmpDoOpt);return this; }
    public IvyFlowBuilderNodeProp cmpDoOptEL(Object cmpDoOptEL){ properties.setCmpDoOptEL(cmpDoOptEL);return this; }
    public IvyFlowBuilderNodeProp cmpBreakOpt(String cmpBreakOpt){ properties.setCmpBreakOpt(cmpBreakOpt);return this; }
    public IvyFlowBuilderNodeProp cmpBreakOptEL(Object cmpBreakOptEL){ properties.setCmpBreakOptEL(cmpBreakOptEL);return this; }
    public IvyFlowBuilderNodeProp cmpDataName(String cmpDataName){ properties.setCmpDataName(cmpDataName);return this; }
    public IvyFlowBuilderNodeProp cmpData(String cmpData){ properties.setCmpData(cmpData);return this; }

    public NodeProperties getProperties() {
        return properties;
    }
}
