package com.ivy.builder.json;


import com.ivy.builder.graph.Edge;
import com.ivy.builder.graph.EdgeProperties;

public class IvyFlowBuilderEdgeProp {

    private EdgeProperties properties;

    public IvyFlowBuilderEdgeProp() {
        this.properties = new EdgeProperties();
    }

    public static IvyFlowBuilderEdgeProp NEW(){
        return new IvyFlowBuilderEdgeProp();
    }

    public IvyFlowBuilderEdgeProp id(String id){
        this.properties.setId(id);
        return this;
    }

    public IvyFlowBuilderEdgeProp tag(String tag){
        this.properties.setTag(tag);
        return this;
    }

    public IvyFlowBuilderEdgeProp linkType(int linkType){
        this.properties.setLinkType(linkType);
        return this;
    }

    public EdgeProperties getProperties() {
        return properties;
    }
}
