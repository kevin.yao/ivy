package com.ivy.builder.graph;

import com.yomahub.liteflow.enums.NodeTypeEnum;
import lombok.Data;

@Data
public class IvyCmp {

    private Long id;

    private String componentId;

    private String componentName;

    private String type;//

    private NodeTypeEnum nodeType;//switch,for

    //0:并行分组,1:串行分组,2:子流程,3:捕获异常,4:与或非
    private Integer groupType;

    private String script;//

    private String language;//

    private String clazz;//

    private Long fallbackId;//

    private String fallbackType;//

    private String el;

    private String elFormat;

    private String cmpPre;
    private Object[] preEL;

    private String cmpFinallyOpt;
    private Object[] finallyEL;

    private String cmpId;

    private String cmpTag;

    private Integer cmpMaxWaitSeconds;

    private String cmpTo;

    private String cmpDefaultOpt;

    private Object cmpToEL;
    private Object cmpDefaultOptEL;

    private String cmpTrueOpt;
    private Object cmpTrueOptEL;

    private String cmpFalseOpt;
    private Object cmpFalseOptEL;

    private Boolean cmpParallel;

    private String cmpDoOpt;
    private Object cmpDoOptEL;

    private String cmpBreakOpt;
    private Object cmpBreakOptEL;

    private String cmpDataName;

    private String cmpData;

    private Object[] andEL;
    private Object[] orEL;
    private Object notEL;
    private Object catchEL;

    private Boolean whenIgnoreError;
    private Boolean whenAny;
    private String whenMust;
}
