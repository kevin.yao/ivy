package com.ivy.builder.graph;

import lombok.Data;

@Data
public class EdgeProperties {

    // 定义节点和边的属性，根据实际情况添加字段
    String id;
    String tag;

    // 0:普通路径，1：switch路径，2：default路径，3：TRUE路径，4：FALSE路径，
    // 5：for路径，6：while路径，7：iterator路径 8:break路径
    Integer linkType = 0;

}
