package com.ivy.builder.graph;

import lombok.Data;

@Data
public class IvyDynamicClass {

    private Long id;

    private String classId;

    private Integer classType;

    private Integer isFallback;

    private String packagePath;

    private String className;

    private Integer classLoaderType;

    private String sourceCode;

    private Integer version;

    private String sourceCmpId;

    private String sourceClassName;

}
