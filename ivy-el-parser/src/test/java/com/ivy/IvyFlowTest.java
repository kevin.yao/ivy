package com.ivy;

import com.ivy.builder.IvyFlowBuilder;
import com.ivy.builder.graph.LogicFlowData;
import com.ivy.builder.json.*;
import com.ivy.parser.IvyFlowParser;
import com.ivy.parser.execption.LiteFlowELException;
import com.yomahub.liteflow.enums.NodeTypeEnum;

public class IvyFlowTest {

    public static void main(String[] args) throws LiteFlowELException {
        //构建节点属性数据
        IvyFlowBuilderNodeProp nodeProp1 = IvyFlowBuilderNodeProp.NEW().componentId("a").componentName("组件a").nodeType(NodeTypeEnum.COMMON);
        IvyFlowBuilderNodeProp nodeProp2 = IvyFlowBuilderNodeProp.NEW().componentId("b").componentName("组件b").nodeType(NodeTypeEnum.COMMON);
        IvyFlowBuilderNodeProp nodeProp3 = IvyFlowBuilderNodeProp.NEW().componentId("c").componentName("组件c").nodeType(NodeTypeEnum.COMMON);
        //构建节点数据(普通组件)
        IvyFlowBuilderNode node1 = IvyFlowBuilderNode.NEW().id().type(NodeTypeEnum.COMMON).properties(nodeProp1);
        IvyFlowBuilderNode node2 = IvyFlowBuilderNode.NEW().id().type(NodeTypeEnum.COMMON).properties(nodeProp2);
        IvyFlowBuilderNode node3 = IvyFlowBuilderNode.NEW().id().type(NodeTypeEnum.COMMON).properties(nodeProp3);
        //构建连线属性数据
        IvyFlowBuilderEdgeProp edgeProp = IvyFlowBuilderEdgeProp.NEW().linkType(0).id("id").tag("tag");
        //构建连线数据
        IvyFlowBuilderEdge edge1 = IvyFlowBuilderEdge.NEW()
                .sourceNode(node2.getNode().getId())
                .targetNode(node3.getNode().getId())
                .text("").properties(edgeProp);

        //获取json数据并解析
        String json = IvyFlowBuilder.builderJson()
                .addNode(node1,node2,node3)
                .addEdge(node1,node2)
                .addEdge(edge1)
                .toFormatJson();
        System.out.println(json);
        String el = IvyFlowParser.parse(json,true);

        System.out.println(el);

        //获取对象数据并解析
//        LogicFlowData flowData = IvyFlowBuilder.builderJson()
//                .addNode(node1,node2,node3)
//                .addEdge(node1,node2)
//                .addEdge(edge1)
//                .toFlowData();
//        System.out.println(json);
//        IvyFlowParser.parse(flowData);
    }

}
